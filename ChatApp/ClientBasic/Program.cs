﻿using System;
using Grpc.Net.Client;

namespace ClientBasic
{
    class Program
    {

        static void Main(string[] args)
        {
            string name;
            string message;
            do
            {
                Console.WriteLine("Type exit to exit program");
                Console.WriteLine("Enter your name: ");
                name = Console.ReadLine();
                Console.WriteLine("Enter your message");

                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Chat.ChatClient(channel);
                var reply = await client.Write(new ChatMessage
                {
                    Name = name,
                    Content = message,
                    Status = true,
                    Time = DateTimeKind.Local
                });
                Console.WriteLine(reply.Message);
            } while (message != "exit");

            Console.ReadLine();

        }
}
