﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ChatApp.Services
{
    public abstract class Message<TRequest, TResponse>
    {
        protected ILogger Logger { get; set; }

        public Message(ILoggerFactory loggerFactory)
        {
            Logger = loggerFactory.CreateLogger<Message<TRequest, TResponse>>();
        }

        public abstract string GetClientId(TRequest message);

        public abstract TResponse Process(TRequest message);
    }
}

