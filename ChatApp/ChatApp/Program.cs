using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Grpc.Core;

namespace ChatApp
{
    public class Program
    {
        const string Host = "localhost";
        const int Port = 1508;

        public static void Main(string[] args)
        {
            // Build server
            var server = new Server
            {
                Services = { Chat.BindService (new Service()) },
                Ports = { new ServerPort (Host, Port, ServerCredentials.Insecure) }
            };

            // Start server
            server.Start();

            Console.WriteLine("ChatServer listening on port " + Port);
            Console.WriteLine("Press any key to stop the server...");
            Console.ReadKey();

            server.ShutdownAsync().Wait();
        }
    }
    
}
