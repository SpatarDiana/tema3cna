﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Grpc.Core;

namespace ChatApp
{
    public class Service : Chat.ChatBase
    {
        private static HashSet<IServerStreamWriter<ChatMessage>> responseStreams = new HashSet<IServerStreamWriter<ChatMessage>>();
        public override async Task Write(IAsyncStreamReader<ChatMessage> requestStream,
              IServerStreamWriter<ChatMessage> responseStream,
              ServerCallContext context)
        {
            // Keep track of connected clients 
            responseStreams.Add(responseStream);

            while (await requestStream.MoveNext(CancellationToken.None))
            {
                // Get the client message from the request stream
                var messageFromClient = requestStream.Current;

                // Create a server message that wraps the client message
                var message = new ChatMessage(messageFromClient);

                // Send to connected clients
                foreach (var stream in responseStreams)
                {
                    await stream.WriteAsync(message);
                }
            }
        }
    }
}