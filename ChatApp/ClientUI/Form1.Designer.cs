﻿namespace ClientUI
{
    partial class Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbox_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbox_message = new System.Windows.Forms.TextBox();
            this.lbl_message = new System.Windows.Forms.Label();
            this.btn_send = new System.Windows.Forms.Button();
            this.txtbox_chat = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtbox_name
            // 
            this.txtbox_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.txtbox_name.Location = new System.Drawing.Point(100, 400);
            this.txtbox_name.MaxLength = 20;
            this.txtbox_name.Name = "txtbox_name";
            this.txtbox_name.Size = new System.Drawing.Size(150, 21);
            this.txtbox_name.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(43, 403);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name :";
            // 
            // txtbox_message
            // 
            this.txtbox_message.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.txtbox_message.Location = new System.Drawing.Point(352, 400);
            this.txtbox_message.MaxLength = 20;
            this.txtbox_message.Name = "txtbox_message";
            this.txtbox_message.Size = new System.Drawing.Size(250, 21);
            this.txtbox_message.TabIndex = 2;
            // 
            // lbl_message
            // 
            this.lbl_message.AutoSize = true;
            this.lbl_message.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lbl_message.ForeColor = System.Drawing.Color.White;
            this.lbl_message.Location = new System.Drawing.Point(275, 403);
            this.lbl_message.Name = "lbl_message";
            this.lbl_message.Size = new System.Drawing.Size(71, 16);
            this.lbl_message.TabIndex = 3;
            this.lbl_message.Text = "Message :";
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(643, 391);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(103, 40);
            this.btn_send.TabIndex = 4;
            this.btn_send.Text = "Send";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // txtbox_chat
            // 
            this.txtbox_chat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.txtbox_chat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtbox_chat.ForeColor = System.Drawing.Color.White;
            this.txtbox_chat.Location = new System.Drawing.Point(46, 28);
            this.txtbox_chat.Name = "txtbox_chat";
            this.txtbox_chat.Size = new System.Drawing.Size(700, 333);
            this.txtbox_chat.TabIndex = 5;
            this.txtbox_chat.Text = "";
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtbox_chat);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.lbl_message);
            this.Controls.Add(this.txtbox_message);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbox_name);
            this.MaximizeBox = false;
            this.Name = "Form";
            this.Text = "Chat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbox_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbox_message;
        private System.Windows.Forms.Label lbl_message;
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.RichTextBox txtbox_chat;
    }
}

