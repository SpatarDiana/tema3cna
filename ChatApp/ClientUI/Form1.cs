﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientUI
{
    public partial class Form : System.Windows.Forms.Form
    {
        private int lastWrittenIndex;
        private const string Host = "localhost";
        private const int Port = 5001;

        private ChatService.ChatServiceClient chat;
        private AsyncDuplexStreamingCall<ChatMessage, ChatMessageFromServer> callService;
        public Form()
        {
            InitializeComponent();
            lastWrittenIndex = 0;
            var channel = new Channel(Host + ":" + Port, ChannelCredentials.Insecure);
            chat = new Chat.ChatServiceClient(channel);
        }

        private async void ChatForm_Load(object sender, EventArgs e)
        {
            try
            {
                using (callService = chat.Write())
                {
                    while (await callService.ResponseStream.MoveNext(CancellationToken.None))
                    {
                        var serverMessage = callService.ResponseStream.Current;
                        var otherClientMessage = serverMessage.Message;

                        int messageStart = txtbox_name.Text.Length + 3;
                        lastWrittenIndex = txtbox_chat.Text.Length + messageStart;

                        txtbox_chat.Text += txtbox_name.Text + " : " + txtbox_message.Text + "\n";
                        ChangeTextBoxText(txtbox_message.Text);
                    }

                }
            }
            catch (RpcException)
            {
                callService = null;
                throw;
            }
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
            var message = new ChatMessage
            {
                Name = txtbox_name,
                Content = txtbox_message,
                Status = true,
                Time = DateTime.Now
            };

            if (callService != null)
            {
                await callService.RequestStream.WriteAsync(message);
            }
        }

        private void ChangeTextBoxText(string message)
        {

            string[] auxMessage = message.Split(' ');

            foreach (string word in auxMessage)
            {
                if (CheckWordSpecialCharacter(word) != ".")
                {
                    SelectWordAndChange(lastWrittenIndex, word, CheckWordSpecialCharacter(word));
                }
                else
                {
                    lastWrittenIndex += word.Length + 1;
                }
            }
        }

        private void SelectWordAndChange(int lastWrittenIndex, string word, string specialCharacter)
        {
            switch (specialCharacter)
            {
                case "*":
                    {           
                        txtbox_chat.Select(lastWrittenIndex, word.Length);
                        txtbox_chat.SelectedText = txtbox_chat.SelectedText.Replace("*", "");

                        txtbox_chat.Select(lastWrittenIndex, word.Length - 2);
                        this.lastWrittenIndex += word.Length - 1;
                        
                        txtbox_chat.SelectionFont = new Font("Microsoft Sans Serif", 10, FontStyle.Bold);
                        txtbox_chat.DeselectAll();
                        break;
                    }

                case "_":
                    {
                        txtbox_chat.Select(lastWrittenIndex, word.Length);
                        txtbox_chat.SelectedText = txtbox_chat.SelectedText.Replace("_", "");

                        txtbox_chat.Select(lastWrittenIndex, word.Length - 2);
                        this.lastWrittenIndex += word.Length - 1;

                        txtbox_chat.SelectionFont = new Font("Microsoft Sans Serif", 10, FontStyle.Italic | FontStyle.Bold);
                        txtbox_chat.DeselectAll();
                        break;
                    }

                case "~":
                    {
                        txtbox_chat.Select(lastWrittenIndex, word.Length);
                        txtbox_chat.SelectedText = txtbox_chat.SelectedText.Replace("~", "");

                        txtbox_chat.Select(lastWrittenIndex, word.Length - 2);
                        this.lastWrittenIndex += word.Length - 1;

                        txtbox_chat.SelectionFont = new Font("Microsoft Sans Serif", 10, FontStyle.Strikeout);
                        txtbox_chat.DeselectAll();
                        break;
                    }

                case "'":
                    {
                        txtbox_chat.Select(lastWrittenIndex, word.Length);
                        txtbox_chat.SelectedText = txtbox_chat.SelectedText.Replace("'", "");

                        txtbox_chat.Select(lastWrittenIndex, word.Length - 2);
                        this.lastWrittenIndex += word.Length - 1;

                        txtbox_chat.SelectionFont = new Font("Microsoft Sans Serif", 10, FontStyle.Underline);
                        txtbox_chat.DeselectAll();
                        break;
                    }

                default: break;
            }

        }

        private string CheckWordSpecialCharacter(string word)
        {
            if (word[0] == '*' && word[word.Length - 1] == '*')
            {
                return "*";
            }

            if (word[0] == '_' && word[word.Length - 1] == '_')
            {
                return "_";
            }

            if (word[0] == '~' && word[word.Length - 1] == '~')
            {
                return "~";
            }

            if (word[0].ToString() == "'" && word[word.Length - 1].ToString() == "'")
            {
                return "'";
            }
            return ".";
        }

    }
}

